<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="user_id", orphanRemoval=true)
     */
    private $event_id;

    public function __construct()
    {
        parent::__construct();
        $this->event_id = new ArrayCollection();
        // your own logic
    }

    public function __toString()
    {
        return (string) $this->getUsername();
    }
}